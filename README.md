## Create a website that count how frequently different letters and words are used in a block of text.
---
> create some new directories
- Index.html
- Javascript
- Readme.md

> Input and get text with
- Textarea
- Button

[Frequency-Counter](https://court181.gitlab.io/frequency-counter)

---
> Add Event Handler to Button
`document.getElementById("countButton").onclick=function(){ your code}`

>Find out what was typed into the textarea (field)
`let typedText=document.getElementById("textInput").value`
- Find a way to ignore capitalization and punctuation.
`typedText=typedText.toLowerCase()`
`typedTest=typedText.replace(/[^a-z'\s]+/g,"")`
- Counting Letters and Words
`for (let i=0;i<typedText; i++){ currentLetter=typedText[i]}`

Reseach --> "associative array"

When encountering a letter for the 1st time, you will initialize the corresponding count to 1.  Otherwise you will add 1 to the existing count.

Output: How many times does each letter occurred. 


How many times does each word occurred.  Use --->
`words=typedText.split(/\s/)
